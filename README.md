# Bayswitch 8-port remote unit firmware

Bayswitch system is a remote controlled antenna farm management system
based on the CANbus network.

This repository provides firmware for an STM32F0*-controlled 8-port
antenna switching remote device.

The project uses the STM32CubIDE development environment, and is designed
to be opened within it.
